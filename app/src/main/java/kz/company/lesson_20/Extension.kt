package kz.company.lesson_20

import android.widget.EditText

fun EditText.checkLetterA(): Boolean =
    this.text.toString().contains('a')

fun String.haveLetterB(): Boolean =
    this.contains('b')

