package kz.company.lesson_20

import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.style.ForegroundColorSpan
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.toSpannable
import androidx.core.widget.addTextChangedListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val spannableText = "Hello World!".toSpannable()
        spannableText.setSpan(
            ForegroundColorSpan(Color.BLUE),
            0,
            4,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        textView.text = spannableText

        editText.addTextChangedListener {
            it?.let {
                textView.text = it.toString()
                if (editText.checkLetterA()) {
                    val spannableText2 = it.toString().toSpannable()
                    spannableText2.setSpan(
                        ForegroundColorSpan(Color.YELLOW),
                        it.length - 1,
                        it.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    textView.text = spannableText2
                }

                if(it.toString().haveLetterB()){

                }
            }

        }
    }
}